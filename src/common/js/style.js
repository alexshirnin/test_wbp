const $ = require('jquery')

$(document).ready(function () {
    if($('body').hasClass('item')){
        anchorsClick();
    } else {
        agreementCheck();
        menuTop();
        anchorsClick();
        menuHeight();
        modelSlider();
        equipmentSlider();
        autoModalTitle();
        // _v2
        modalCustom();
        phoneMask();
    }

    $('#wrapMap').length ? mapHolder() : false;



    $(window).on('resize', function(){
        menuHeight();
        equipmentSlider();
    }).trigger('resize');

})// ready

$(window).on('load', function(){
    if($(window).width() > 1366) {
        if ($(window).scrollTop() >= 200) {
            $('.menu-top').addClass('menu-shrink');
        } else {
            $('.menu-top').removeClass('menu-shrink');
        }
    }

    const observer = new IntersectionObserver((entries, observer) => {
        entries.forEach(entry => {
            if (entry.isIntersecting) {
                entry.target.src = entry.target.dataset.src
                observer.unobserve(entry.target)
            }
        })
    }, { threshold: 0.5 })
    document.querySelectorAll('img[data-src]').forEach(img => observer.observe(img));

    $('.section-top--about').addClass('active')
}).trigger('load');

function menuTop(){
    let menu = $('.menu-top--nav');
    let menuLink = $('.menu-top--nav-link');
    let menuBtn = $('.js-mob-btn');
    let btnOpen = $('.js-mob-btn .menu-open');
    let btnClose = $('.js-mob-btn .menu-close');
    btnOpen.addClass('active');

    function menuShow(){
        btnOpen.removeClass('active');
        menu.addClass('active');
        btnClose.addClass('active');
        $('body').addClass('menu-show');
    }
    function menuHide(){
        btnOpen.addClass('active');
        menu.removeClass('active');
        btnClose.removeClass('active');
        $('body').removeClass('menu-show');
    }

    menuBtn.click(function (){
        if(btnOpen.hasClass('active')){
            menuShow();
        } else {
            menuHide();
        }
    });

    menuLink.click(function (){
        menuHide();
    });

    $(document).mouseup( function(e){
        var div = $( ".menu-top--nav-overlay" );
        if ( div.is(e.target) && div.has(e.target).length === 0 ) {
            menuHide();
        }
    });
}
function menuHeight(){
    if($(window).width() > 1366) {
        $(window).scroll(function () {
            if ($(window).scrollTop() >= 200) {
                $('.menu-top').addClass('menu-shrink');
            } else {
                $('.menu-top').removeClass('menu-shrink');
            }
        });
    }
}
function mapHolder() {
// создаём элемент <div>, который будем перемещать вместе с указателем мыши пользователя
    let mapTitle = document.createElement('div');
    mapTitle.className = 'mapTitle';
// вписываем нужный нам текст внутрь элемента
    mapTitle.textContent = 'Для активации карты нажмите на неё';
// добавляем элемент с подсказкой последним элементов внутрь нашего <div> с id wrapMap
    wrapMap.appendChild(mapTitle);
// по клику на карту
    wrapMap.onclick = function () {
        // убираем атрибут "style", в котором прописано свойство "pointer-events"
        this.children[0].removeAttribute('style');
        // удаляем элемент с интерактивной подсказкой
        mapTitle.parentElement.removeChild(mapTitle);
    }
// по движению мыши в области карты
    wrapMap.onmousemove = function (event) {
        // показываем подсказку
        mapTitle.style.display = 'block';
        // двигаем подсказку по области карты вместе с мышкой пользователя
        if (event.offsetY > 10) mapTitle.style.top = event.offsetY + 20 + 'px';
        if (event.offsetX > 10) mapTitle.style.left = event.offsetX + 20 + 'px';
    }
// при уходе указателя мыши с области карты
    wrapMap.onmouseleave = function () {
        // прячем подсказку
        mapTitle.style.display = 'none';
    }
}
function agreementCheck() {
    const forms = $('[data-form^="form"]');

    forms.each(function() {
        const form = $(this);
        const button = form.find('button[type="submit"]');
        const checkboxGroup = form.find('.js-agreement input[type="checkbox"]');
        const phone = form.find('input[type="tel"]');
        const agreements = form.find('.js-agreement');

        button.prop('disabled', true);
        form.find('.js-btn-submit').addClass('js-disabled');

        form.on('click', '.js-btn-submit.js-disabled', function() {
            if (phone.val().length < 18) {
                phone.addClass('error');
            } else {
                phone.removeClass('error');
                agreements.removeClass('trembling');
                setTimeout(() => agreements.addClass('trembling'), 100);
            }
        });

        function updateButtonState() {
            const allChecked = checkboxGroup.length && checkboxGroup.filter(':checked').length === checkboxGroup.length;
            const isPhoneValid = phone.val().length >= 18;

            button.prop('disabled', !(isPhoneValid && allChecked));
            form.find('.js-btn-submit').toggleClass('js-disabled', !(isPhoneValid && allChecked));
        }

        checkboxGroup.on('change', function() {
            const isPhoneValid = phone.val().length >= 18;
            const isChecked = $(this).is(':checked');

            $(this).parents('.js-agreement').toggleClass('no-trembling', isChecked);
            updateButtonState();
        });

        phone.on('keyup', function() {
            const isPhoneValid = phone.val().length >= 18;

            phone.toggleClass('error', !isPhoneValid);
            updateButtonState();
        });
    });
}

function anchorsClick() {
    $('.menu-top--nav-link[href^="#"]').on("click", function (event) {
        event.preventDefault();
        let winW = $(window).width();
        let menu = $('.menu-top').height();
        let id  = $(this).attr('href');
        if (winW < 768){
            if (location.pathname.replace(/^\//, '') === this.pathname.replace(/^\//, '') && location.hostname === this.hostname) {
                let target = $(this.hash);
                target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
                if (target.length) {
                    if(id === '#anchor04'){
                        $('html,body').animate({scrollTop: target.offset().top - menu + 300}, 500);
                        return false;
                    }
                    else if(id === '#anchor05'){
                        $('html,body').animate({scrollTop: target.offset().top - menu + 230}, 500);
                        return false;
                    } else {
                        $('html,body').animate({scrollTop: target.offset().top - menu}, 500);
                        return false;
                    }
                }
            }
        }
    });
}
function modelSlider() {
    $('.models-images').each(function() {
        const slider = $(this);
        const dotsContainer = slider.find('.js-models-dots');

        slider.find('.models-slider--image').each(function() {
            const modelColor = $(this).attr('data-color');
            const dot = $('<div class="models-slider--dot"><span class="models-slider--dot-bg"></span></div>');
            dot.find('.models-slider--dot-bg').attr('data-color', modelColor);
            dotsContainer.append(dot);
        });

        const dots = dotsContainer.find('.models-slider--dot');
        const images = slider.find('.models-slider--image');

        images.first().addClass('active');
        dots.first().addClass('active');

        dots.click(function() {
            const dotColor = $(this).find('.models-slider--dot-bg').attr('data-color');
            dots.removeClass('active');
            images.removeClass('active');
            $(this).addClass('active');
            images.filter(`[data-color="${dotColor}"]`).addClass('active');
        });
    });
}

function equipmentSlider(){
    if($(window).width() > 1199) {
        $('.equipment-slider').trigger('destroy.owl.carousel');
    }
    else {
        $('.equipment-slider').owlCarousel({
            loop: true,
            margin: 0,
            stagePadding: 50,
            nav: false,
            dots: true,
            autoplay:false,
            responsive:{
                0:{
                    items: 1,
                    stagePadding: 0,
                    margin: 5,
                },
                370:{
                    items: 1,

                },
                650:{
                    items: 2,
                }
            }
        });
    }
}
function autoModalTitle(){
    $('.page-btn').not('[type="submit"]').click(function (){
        let modalTitle = $(this).text();
        $('#callModal .modal-title').html('').append(modalTitle)
    });
}

function modalCustom(){
    $(".modal-btn").click(function() {
        const modalId = $(this).data("modal-id");
        const modal = $("#" + modalId);
        modal.addClass("modal-open");
        $('body').addClass('overflow').css('margin-right', getScrollbarWidth());

        console.log('dfxdxd')
    });

    $(".close").click(function() {
        const modal = $(this).parents('.modal.modal-open');
        modal.removeClass("modal-open");
        $('body').removeClass('overflow').css('margin-right', 0);
        $('.js-agreement input[type="checkbox"]').each(function (){
            $(this).prop('checked', false);
        });
        $('.js-btn-submit').each(function (){
            $(this).addClass('js-disabled');
            $(this).find('button[type="submit"]').prop('disabled', true);
        });
    });

    $(window).click(function(e) {
        $(".modal").each(function() {
            if ($(this)[0] === e.target) {
                $(this).removeClass("modal-open");
                $('body').removeClass('overflow').css('margin-right', 0);
                $('.js-agreement input[type="checkbox"]').each(function (){
                    $(this).prop('checked', false);
                });
                $('.js-btn-submit').each(function (){
                    $(this).addClass('js-disabled');
                    $(this).find('button[type="submit"]').prop('disabled', true);
                });
            }
        });
    });
}
// вычисление ширины полосы прокрутки
function getScrollbarWidth() {
    let div = $('<div>').css({
        position: 'absolute',
        top: '-9999px',
        width: '50px',
        height: '50px',
        overflow: 'scroll'
    }).appendTo('body');

    let scrollbarWidth = div[0].offsetWidth - div[0].clientWidth;

    div.remove();

    return scrollbarWidth;
}
// /вычисление ширины полосы прокрутки

function phoneMask() {
    $('input[type="tel"]').on('input', function() {
        const phone = $(this).val().replace(/\D/g, '').substring(0, 11);
        const countryCode = phone.substring(0, 1);
        const areaCode = phone.substring(1, 4);
        const areaPhone1 = phone.substring(4, 7);
        const areaPhone2 = phone.substring(7, 9);
        const areaPhone3 = phone.substring(9, 11);

        let formattedPhone = '';

        if (countryCode === '9') {
            formattedPhone = '+7 (9';
        } else if (countryCode >= '0' && countryCode <= '8') {
            formattedPhone = '+7';
        } else {
            formattedPhone = '+' + countryCode;
        }

        if (phone.length > 1) {
            formattedPhone += ` (${areaCode}`;
        }

        if (phone.length > 4) {
            formattedPhone += `) ${areaPhone1}`;
        }

        if (phone.length > 7) {
            formattedPhone += `-${areaPhone2}`;
        }

        if (phone.length > 9) {
            formattedPhone += `-${areaPhone3}`;
        }

        $(this).val(formattedPhone);
    });
}


